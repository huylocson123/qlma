// https://62b07874e460b79df0469ac8.mockapi.io/
import { monAnService } from "./service/monAnService.js";

let foodList = [];
let renderTable = (list) => {
    let contentHTML = "";
    for (let index = 0; index < list.length; index++) {
        let monAn = list[index];
        let contentTr = `<tr> 
                         <td> ${monAn.id} </td>
                         <td> ${monAn.name} </td>
                         <td> ${monAn.price} </td>
                         <td> ${monAn.description} </td>                   
                         <td> 
                            <button class="btn btn-danger" onclick="deleteFood(${monAn.id})">Xoá</button>
                            <button class="btn btn-primary"  onclick="loadToForm(${monAn.id})">Sửa</button>
                         </td>
                     </tr>`;
        contentHTML = contentHTML + contentTr;
    }
    document.getElementById("tbody_food").innerHTML = contentHTML;
    // render danh sách món ăn ra ngoài màn hình
};

let deleteFood = (idFood) => {
    monAnService
        .deleteGood(idFood)
        .then((res) => {
            console.log(res);
            loadFood();
        })
        .catch((error) => {
            console.log(error);
        });
};
window.deleteFood = deleteFood;

// UPdate

let loadToForm = (idFood) => {
    monAnService
        .getFoodById(idFood)
        .then((res) => {
            let foodData = res.data;
            document.getElementById("name").value = foodData.name;
            document.getElementById("price").value = foodData.price;
            document.getElementById("des").value = foodData.description;
            document.getElementById("idFood").value = foodData.id;
        })
        .catch((error) => {
            console.log(error);
        });
};
window.loadToForm = loadToForm;

let updateFood = () => {
    let name = document.getElementById("name").value;
    let price = document.getElementById("price").value;
    let des = document.getElementById("des").value;
    let idFood = document.getElementById("idFood").value;
    let food = {
        name: name,
        price: price,
        description: des,
    };
    monAnService
        .updateFood(idFood, food)
        .then((res) => {
            loadFood();
        })
        .catch((error) => {
            console.log(error);
        });
};
window.updateFood = updateFood;

// ADD
let addFood = () => {
    let name = document.getElementById("name").value;
    let price = document.getElementById("price").value;
    let des = document.getElementById("des").value;
    let food = {
        name: name,
        price: price,
        description: des,
    };
    monAnService
        .addFood(food)
        .then(() => {
            loadFood();
        })
        .catch((error) => {
            alert(error);
        });
};
window.addFood = addFood;

let loadFood = () => {
    monAnService
        .getListFood()
        .then((res) => {
            foodList = res.data;
            renderTable(foodList);
        })
        .catch((err) => {
            console.log(err);
        });
};

loadFood();