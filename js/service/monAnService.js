export let monAnService = {
    getListFood: () => {
        return axios({
            url: "https://62b740320d4a2cd3e1a96fea.mockapi.io/mon-an",
            method: "GET",
        });
    },
    deleteGood: (idFood) => {
        return axios({
            url: `https://62b740320d4a2cd3e1a96fea.mockapi.io/mon-an/${idFood}`,
            method: "DELETE",
        });
    },
    addFood: (food) => {
        return axios({
            url: "https://62b740320d4a2cd3e1a96fea.mockapi.io/mon-an",
            method: "POST",
            data: food,
        });
    },
    getFoodById: (idFood) => {
        return axios({
            url: `https://62b740320d4a2cd3e1a96fea.mockapi.io/mon-an/${idFood}`,
            method: "GET",
        });
    },

    updateFood: (idFood, food) => {
        return axios({
            url: `https://62b740320d4a2cd3e1a96fea.mockapi.io/mon-an/${idFood}`,
            method: "PUT",
            data: food,
        });
    },
};